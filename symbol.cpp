#include "global.hpp"
#include "parser.hpp"

using namespace std;

vector<Symbol> SymbolTable;

//Licznik zmiennych tymczasowych np. t0,t1,t2
int tempVariableCounter = 0;
//Licznik labeli w kodzie lab0,lab1 itp
int labelCounter = 1;


int getSymbolTableSize() {
	return (int) (SymbolTable.size() - 1);
}

//Wstawia nowy element do tablicy symboli i zwraca index włozenego elementu
int insert(string name, int token, int type, bool isReference, int address) {
	Symbol symbol;
	symbol.token = token;
	symbol.name = name;
	symbol.type = type;
	symbol.isGlobal = isGlobalScope;
	symbol.isReference = isReference;
	symbol.address = address;

	SymbolTable.push_back(symbol);
	return getSymbolTableSize();
}

//Wstawia cyfre do tablicy symboli jesli jej nie znajdzie, zwraca index tego elementu
// Jezeli cyfra nie istnieje w tablicy symboli to wstawia ja do tablicy symboli.
// Zwraca index elementu w tablicy symboli
int insertNum(string val, int type) {
	int num = lookup(val);

	if (num == -1) {
		num = insert(val, NUM, type, false, 0);
	}
	return num;
}

//Zwraca pierwszy wolny adres ktory moze byc wykorzystany
int getFirstEmptyAddress(string symbolName) {
	int address = 0;

	for (auto &symbol : SymbolTable) {
		if(isGlobalScope && symbol.isGlobal && symbol.name != symbolName) {
			address += getSymbolSize(symbol);
		}
		if(!isGlobalScope && !symbol.isGlobal && symbol.address <= 0) {
			address -= getSymbolSize(symbol);
		}
	}
	return address;
}

//Wstawia zmienna tymczasowa do tablicy symboli, okresla jej adres, i zwraca index wstawionego elementu
int insertTempSymbol(int type) {
	string name = "$t" + to_string(tempVariableCounter++);
	int id = insert(name, VAR, type, false, 0);
	SymbolTable[id].address = getFirstEmptyAddress(name);
	return id;
}

int insertLabel() {
	string name = "lab" + to_string(labelCounter++);
	return insert(name, LABEL, NONE, false, 0);
}

void initSymbolTable() {
	Symbol lab0;
	lab0.name = ("lab0");
	lab0.isGlobal = true;
	lab0.isReference = false;
	lab0.token = LABEL;
	SymbolTable.push_back(lab0);
}

//Zwraca index w tablicy symboli na podstawie nazwy elementu
int lookup(string name) {
	int index = getSymbolTableSize();

	for (; index >= 0; index--) {
		if (SymbolTable[index].name == name) {
			return index;
		}
	}
	return -1;
}

//Zwraca index w tablicy symboli na podstawie nazwy elementu
int lookupIfExist(string name) {
	int index = getSymbolTableSize();

	for (; index >= 0; index--) {
		if(SymbolTable[index].name == name){
			if(isGlobalScope || (!isGlobalScope && !SymbolTable[index].isGlobal)){
				return index;
			}
		}
	}

	return -1;
}

//Szuka czy element istnieje w tablicy symboli jesli nie to go wstawia i zwraca jego index
int lookupIfExistAndInsert(string s, int token, int type) {
	int index = lookupIfExist(s);

	if (index == -1) {
		index = insert(s, token, type, false, 0);
	}
	return index;
}

//Szuka czy funkcja o danej nazwie istnieje w tablicy symboli, jesli tak zwraca jej index
int lookupForFunction(string functionName) {
	int index = getSymbolTableSize();
	
	for (; index >= 0; index--) {
		if (SymbolTable[index].name == functionName && (SymbolTable[index].token == FUN || SymbolTable[index].token == PROC)) {
			return index;
		}
	}
	return -1;
}

int getSymbolSize(Symbol symbol) {
	const int intSizeElement = 4;
	const int realSizeElement = 8;
	const int referenceSizeElement = 4;
	const int nothingSizeElement = 0;

	if(symbol.token == VAR && symbol.type == INTEGER) return intSizeElement;
	if(symbol.token == VAR && symbol.type == REAL) return realSizeElement;
	if(symbol.isReference) return referenceSizeElement;
	return nothingSizeElement;
}

//czyscimy zmienne lokalne
void clearLocalSymbols() {
	int index = 0;

	for (auto &element : SymbolTable) {
		if (element.isGlobal) {
			index++;
		}
	}
	SymbolTable.erase(SymbolTable.begin() + index, SymbolTable.end());
}

string tokenToString(int token) {
	switch (token) {
		case LABEL:
			return "label";
		case VAR:
			return "variable";
		case NUM:
			return "number";
		case INTEGER:
			return "integer";
		case REAL:
			return "real";
		case PROC:
			return "procedure";
		case FUN:
			return "function";
		case ID:
			return "id";
		default:
			return "null";
	}
}

void printSymbolTable() {
	cout << "; Symbol table " << endl;
	int i = 0;

	for (auto &e : SymbolTable) {
		if (e.token != ID) {
			cout << "; " << i++;

			if (e.isGlobal) {
				cout << " Global ";
			} else {
				cout << " Local ";
			}

			if (e.isReference) {
				cout << "reference variable " << e.name << " ";
				cout << tokenToString(e.type) << " offset=" << e.address << endl;
			} else if (e.token == NUM) {
				cout << tokenToString(e.token) << " " << e.name << " " << tokenToString(e.type) << endl;
			} else if (e.token == VAR) {
				cout << tokenToString(e.token) << " " << e.name << " " << tokenToString(e.type) << " offset="
					 << e.address << endl;
			} else if (e.token == PROC  || e.token == LABEL) {
				cout << tokenToString(e.token) << " " << e.name << " " << endl;
			} else if (e.token == FUN ) {
				cout << tokenToString(e.token) << " " << e.name << " " << tokenToString(e.type) << endl;
			}
		}
	}
}
