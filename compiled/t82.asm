        jump.i  #lab0                   ;jump.i  lab0
f:
        enter.i #28                     ;enter.i 
        mov.i   #4,BP-4                          
        add.r *BP+16,*BP+12,BP-12
        inttoreal.i BP-4,BP-28                                 
        add.r BP-12,BP-28,BP-20
        mov.r   BP-20,*BP+8                      
        leave                           ;leave   
        return                                   
lab0:
        mov.r   #3.25,8                          
        push.i  #8                      ;push.i  &g
        mov.r   #10.28,24                        
        push.i  #24                     ;push.i  &$t3
        push.i  #32                     ;push.i  &$t4
        call.i  #f                      ;call.i  &f
        incsp.i #12                     ;incsp.i 12
        write.r 32                                 
        exit                            ;exit    