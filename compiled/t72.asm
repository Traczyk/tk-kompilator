        jump.i  #lab0                   ;jump.i  lab0
g:
        enter.i #32                     ;enter.i 32
        inttoreal.i #3,BP-8             ;inttoreal.i 3,$t0
        mul.r   *BP+12,BP-8,BP-16       ;mul.r   a,$t0,$t1
        inttoreal.i #15,BP-24           ;inttoreal.i 15,$t2
        sub.r   BP-16,BP-24,BP-32       ;sub.r   $t1,$t2,$t3
        mov.r   BP-32,*BP+8             ;mov.r   $t3,g
        leave                           ;leave   
        return                          ;return  
f:
        enter.i #36                     ;enter.i 36
        mov.i   #4,BP-4                 ;mov.i   4,q
        inttoreal.i BP-4,BP-12          ;inttoreal.i q,$t4
        mul.r   *BP+16,BP-12,BP-20      ;mul.r   a,$t4,$t5
        push.i  BP+12                   ;push.i  &b
        push.i  #BP-28                  ;push.i  &$t6
        call.i  #g                      ;call.i  &g
        incsp.i #8                      ;incsp.i 8
        add.r   BP-20,BP-28,BP-36       ;add.r   $t5,$t6,$t7
        mov.r   BP-36,*BP+8             ;mov.r   $t7,f
        leave                           ;leave   
        return                          ;return  
lab0:
        mov.r   #3.25,8                 ;mov.r   3.25,u
        push.i  #8                      ;push.i  &u
        push.i  #8                      ;push.i  &u
        push.i  #24                     ;push.i  &$t8
        call.i  #f                      ;call.i  &f
        incsp.i #12                     ;incsp.i 12
        write.r 24                      ;write.r $t8
        exit                            ;exit    
