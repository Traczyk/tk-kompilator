        jump.i  #lab0                   ;jump.i  lab0
lab0:
        jl.i    0,4,#lab1
        mov.i   #0,36                            
        jump.i  #lab2                            
lab1:
        mov.i   #1,36                            
lab2:
        jl.i    4,8,#lab3
        mov.i   #0,40                            
        jump.i  #lab4                            
lab3:
        mov.i   #1,40                            
lab4:
        jl.i    0,8,#lab5
        mov.i   #0,44                            
        jump.i  #lab6                            
lab5:
        mov.i   #1,44                            
lab6:
        or.i    40,44,48
        and.i    36,48,52
        je.i    52,#0,#lab7
        mov.r   #0.0,12                          
        jump.i  #lab8                            
lab7:
        mov.r   #1.0,12                          
lab8:
        write.r 12                                 
        exit                            ;exit    