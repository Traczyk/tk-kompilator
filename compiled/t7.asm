        jump.i  #lab0                   ;jump.i  lab0
g:
        enter.i #32                     ;enter.i 
        inttoreal.i #3,BP-16                                 
        mul.r    *BP+12,BP-16,BP-8
        inttoreal.i #15,BP-32                                 
        sub.r BP-8,BP-32,BP-24
        mov.r   BP-24,*BP+8                      
        leave                           ;leave   
        return                                   
f:
        enter.i #36                     ;enter.i 
        mov.i   #4,BP-4                          
        inttoreal.i BP-4,BP-20                                 
        mul.r    *BP+16,BP-20,BP-12
        push.i  BP+12                   ;push.i  &b
        push.i  #BP-28                  ;push.i  &$t6
        call.i  #g                      ;call.i  &g
        incsp.i #8                      ;incsp.i 8
        add.r BP-12,BP-28,BP-36
        mov.r   BP-36,*BP+8                      
        leave                           ;leave   
        return                                   
lab0:
        mov.r   #3.25,8                          
        push.i  #8                      ;push.i  &u
        push.i  #8                      ;push.i  &u
        push.i  #24                     ;push.i  &$t8
        call.i  #f                      ;call.i  &f
        incsp.i #12                     ;incsp.i 12
        write.r 24                                 
        exit                            ;exit    