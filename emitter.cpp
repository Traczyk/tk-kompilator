#include <iomanip>
#include "global.hpp"
#include "parser.hpp"

using namespace std;
using std::cout;
using std::setw;

//zapis do pliku
extern ofstream outputStream;
stringstream ss;


// Na podstawie przekazanych indeksow, sprawdzamy jakiego typu (INTEGER lub REAL) ma byc wynik
int getResultType(int firstIndex, int secondIndex) {
	if (SymbolTable[firstIndex].type == REAL || SymbolTable[secondIndex].type == REAL) {
		return REAL;
	} else {
		return INTEGER;
	}
}

int compareObjects(string value1, string value2) {
	if(value1.compare(value2) == 0) return true;
	return false;
}

int getToken(string value) {
	if(compareObjects(value, "+")) return PLUS;
	if(compareObjects(value, "-")) return MINUS;
	if(compareObjects(value, "*")) return MUL;
	if(compareObjects(value, "div")) return DIV;
	if(compareObjects(value, "/")) return DIV;
	if(compareObjects(value, "mod")) return MOD;
	if(compareObjects(value, "and")) return AND;
	if(compareObjects(value, "=")) return EQ;
	if(compareObjects(value, ">=")) return GE;
	if(compareObjects(value, "<=")) return LE;
	if(compareObjects(value, "<>")) return NE;
	if(compareObjects(value, ">")) return G;
	if(compareObjects(value, "<")) return L;
	return 0;
}

// zwracamy typ symbolu, jezeli jest to wartosc to zwracamy typ przypisany w tablicy symboli.
// jezeli odwolujemy sie przez adres to zwracamy typ INTEGER. Bo adres jest typu INTEGER!
int getSymbolType(int index, bool isValue) {
	if (isValue) {
		return SymbolTable[index].type;
	} else { //address
		return INTEGER;
	}
}

bool checkIfTypeIsCorrect(int type){
	return (type == REAL || type == INTEGER);
}

// jezeli typy dwoch obiektow nie sa takie same, musimy je przekonwertowac do tego samego typu.
// zawsze konwertujemy do REAL, jezeli jeden z obiektow jest typu REAL a drugi typu INTEGER.
// tworzymy nowa zmienna tymczasowa w tablicy symboli, nastepnie przypisujemy do firstVariableIndex lub secondVariableIndex index zmiennej tymczasowej
void castToSameType(int &firstVariableIndex, bool isValue1, int &secondVariableIndex, bool isValue2) {
	int type1 = getSymbolType(firstVariableIndex, isValue1);
	int type2 = getSymbolType(secondVariableIndex, isValue2);

	if(!checkIfTypeIsCorrect(type1) || !checkIfTypeIsCorrect(type2)){
		yyerror("Nieodpowiednie typy do przekonwertowania!");
	}

	if (type1 != type2) {
		int tempVariableIndex = insertTempSymbol(REAL);
		if(type1 == INTEGER) {
			myGenCode(INTTOREAL, tempVariableIndex, isValue1, firstVariableIndex, isValue1, -1, true);
			firstVariableIndex = tempVariableIndex;
		} else {
			myGenCode(INTTOREAL, tempVariableIndex, isValue2, secondVariableIndex, isValue2, -1, true);
			secondVariableIndex = tempVariableIndex;
		}
	}
}

//Pobiera to co jest w tablicy symboli na podstawie indexu i w zaleznosci od tego czy to zmienna globalna lub lokalna
//zwraca pod jakim adresem bedzie dostepna zmienna np. BP+16 / BP-16, jezeli jestemy we funkcji lub sam adres
string prepareVariableAddress(Symbol symbol) {
	string result = "";

	if (!symbol.isGlobal) {
		result = "BP";
		if (symbol.address >= 0) {
			result += "+";
		}
	}
	return result + to_string(symbol.address);
}


string prepareVariable(int index, bool isValue) {
	string result = "";
	Symbol symbol = SymbolTable[index];

	if (symbol.token == NUM) {
		result = "#" + symbol.name;
	}
	else if (symbol.isReference) {

		if (isValue) result = "*";
		result += prepareVariableAddress(symbol);
	}
	else if (symbol.token == VAR) { // example: push.i #BP-32
		if (!isValue) result = "#";
		result += prepareVariableAddress(symbol);
	}
	return result;
}

void writeToOutput(string str) {
	ss << "\n" << str;
}

void writeToOutputExt(string str0, string str1, string str2, string str3, string str4) {
	ss << "\n"
	   << std::setw(8) << std::left << str0
	   << std::setw(8) << std::left << str1
	   << std::setw(24) << std::left << str2
	   << std::setw(9) << std::left << str3
	   << str4;
}

void writeToFile() {
	outputStream.write(ss.str().c_str(), ss.str().size());
	ss.str(string()); //clear
}

//Zwraca koncowke dla typu jaka jest w assemblerze
string getTypeExtInAssembler(int type){
	if (type == REAL) {
		return ".r ";
	} else {
		return  ".i ";
	}
}

void prepareConditionInAssembler(int token){
	ss << "\n        ";

	if (token == EQ){
		ss << "je"; // Jump Equal
	}else if (token == NE){
		ss << "jne"; // Jump not equal
	}else if (token == LE){ 
		ss << "jle"; // Jump Less/Equal
	}
	else if (token == GE){
		ss << "jge"; // Jump Greater/Equal
	}
	else if (token == G){
		ss << "jg";  //Jump Greater
	}else if (token == L){
		ss << "jl"; // Jump Less
	}	
}

void prepareMulopInAssembler(int token){
	ss << "\n        ";
	if (token == MUL) {
		ss << "mul";
	} else if (token == DIV) {
		ss << "div";
	} else if (token == MOD) {
		ss << "mod";
	} else if (token == AND) {
		ss << "and";
	} else if (token == OR) {
		ss << "or";
	}
}

void tokenAssignLogic(int firstVariableIndex, bool isValue1, int secondVariableIndex, bool isValue2, string type){
	int type1 = getSymbolType(firstVariableIndex, isValue1);
	int type2 = getSymbolType(secondVariableIndex, isValue2);

	if(type1 == type2){
		writeToOutputExt("","mov" + type , prepareVariable(secondVariableIndex, isValue2) + "," + prepareVariable(firstVariableIndex, isValue1),"","");
	} else {
		if(type1 == INTEGER && type2 == REAL){
			myGenCode(REALTOINT, firstVariableIndex, isValue1, secondVariableIndex, isValue2, -1, true);
		} else if (type1 == REAL && type2 == INTEGER) {
			myGenCode(INTTOREAL, firstVariableIndex, isValue1, secondVariableIndex, isValue2, -1, true);
		} else {
			yyerror("Nieodpowiednie typy dla przypisania - token ASSIGN");
			writeToOutputExt("","mov" + type , prepareVariable(secondVariableIndex, isValue2) + "," + prepareVariable(firstVariableIndex, isValue1),"","");
		}
	}
}

void tokenReturnLogic(){
	writeToOutputExt("", "return", "", "", "");
	string all = ss.str();
	ss.str(string()); //clear
	size_t find_res = all.find("??");
	ss << -1 * getFirstEmptyAddress(string(""));
	all.replace(find_res, 2, ss.str());
	outputStream.write(all.c_str(), all.size());
	ss.str(string()); //clear
}

// 
void myGenCode(int token, int var1, bool isValue1, int var2, bool isValue2, int var3, bool isValue3) {
	string type;

	if (var1 != -1) {
		type = getTypeExtInAssembler(SymbolTable[var1].type);
	}

	if (token == READ) {
		writeToOutputExt("","read" + type + prepareVariable(var1, isValue1),"","","");
	} else if (token == WRITE) {
		writeToOutputExt("","write" + type + prepareVariable(var1, isValue1),"","","");
	} else if (token == PROC || token == FUN) {
		writeToOutput(SymbolTable[var1].name + ":");
		writeToOutputExt("", "enter.i", "#??", ";enter.i", "");
	} else if (token == LABEL) {
		writeToOutput(SymbolTable[var1].name + ":");
	} else if (token == PUSH) {
		writeToOutputExt("","push.i", prepareVariable(var1, isValue1),";push.i","&"+SymbolTable[var1].name);
	} else if (token == INCSP) {
		writeToOutputExt("","incsp" + type,prepareVariable(var1, isValue1),";incsp" + type + (SymbolTable[var1].name),"");
	} else if (token == JUMP) {
		writeToOutputExt("","jump.i","#" + SymbolTable[var1].name,"", "");
	} else if (token == CALL) {
		writeToOutputExt("","call.i","#" + SymbolTable[var1].name,";call.i","&" + SymbolTable[var1].name);
	} else if (token == REALTOINT) {
		writeToOutputExt("","realtoint.r ",prepareVariable(var2, isValue2) + "," + prepareVariable(var1, isValue1),"","");
	} else if (token == INTTOREAL) {
		writeToOutputExt("", "inttoreal.i " + prepareVariable(var2, isValue1) + "," + prepareVariable(var1, isValue1),"", "",  "");
	} else if (token == EQ || token == NE || token == LE || token == GE || token == G || token <= L) {
		castToSameType(var2, isValue2, var3, isValue3);
		prepareConditionInAssembler(token);
		type = getTypeExtInAssembler(SymbolTable[var1].type);
		ss << type << "   " << prepareVariable(var2, isValue2) << "," << prepareVariable(var3, isValue3)  << "," << "#" << SymbolTable[var1].name;
	} else if (token == PLUS || token == MINUS) {
		castToSameType(var2, isValue2, var3, isValue3);
		ss << "\n        ";
		if (token == MINUS) {
			ss << "sub" << type;
		} else if (token == PLUS) {
			ss << "add" << type;
		}

		//Instrukcja ponizej realizuje zapis np. add.i *BP+16,#10,BP-4
		//sub.i 0, #20, 24
		//var1 - zmienna tymczasowa, var2 - 0, var3 - term
		//zapisumey liczbe ujemna poprzez odjecie dodatniej liczby od zera i zapisanie tego w zmiennej tymczasowej
		ss << prepareVariable(var2, isValue2) << "," << prepareVariable(var3, isValue3) << "," << prepareVariable(var1, isValue1);
	} else if (token == MUL || token == DIV || token == MOD || token == AND || token == OR) {
		castToSameType(var2, isValue2, var3, isValue3);
		prepareMulopInAssembler(token);
		ss << type << "   " << prepareVariable(var2, isValue2) << "," << prepareVariable(var3, isValue2) << "," << prepareVariable(var1, isValue1);
	} else if (token == ASSIGN) {
		tokenAssignLogic(var1, isValue1, var3, isValue3, type);

	} else if (token == RETURN) {
		tokenReturnLogic();
	}
}
