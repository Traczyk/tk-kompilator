#ifndef INCLUDE_H

#define INCLUDE_H

#include <string>
#include <iostream>
#include <list>

using namespace std;

class Symbol {
public:
	bool isReference;		// informacja do tego czy odwolujemy sie przez adres elementu
	bool isGlobal;			// true - jezeli jest to element globalny, zawarty w kodzie glownym programu. 
	int token;              // typ tokenu: FUN, PROC ... 
	int type;               // rodzaj wartości INTEGER/REAL
	int address;
	string name;                                // numer dla liczb lub nazwa: lab0
	list<int> parameters;	// typy parametrów dla funkcji / procedury, a typy sa typu integer
};

#endif
