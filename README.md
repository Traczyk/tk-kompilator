# TK-Kompilator


Kompilujmy komendą “make” : musimy być w tym folderze w ktorym mamy pliki kompilatora.


Po poprawnej kompilacji pojawi nam się plik “comp”
W folderze “inputs” znajdują się przykładowe programy w Pascalu które nasz kompilator zamieni na kod w asemblerze

Przykład:

Chcemy przekompilować plik  t0.pas znajdujący się w inputs:

Komenda:  ./comp inputs/t0.pas  output

Gdzie: 
comp -> nazwa pliku wygenerowanego komendą make
inputs/t0.pas -> ścieżka do programu w pascalu
output -> nazw pliku wynikowego ktory zostanie przekompilowany, możemy go nazwać jak chcemy
