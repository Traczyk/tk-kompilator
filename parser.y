%{
	#include "global.hpp"

	using namespace std;

	list<int> paramHelper;

	int startOffsertParamtersFunProcHelper = 8;	// 8 dla proc 12 dla fun

	//Lista ktora przechowuje index z tablisy symboli
	vector<int> argParamVectorHelper;

	//Lista indexow argumentow funkcji (czyli te elementy ktore funkcja przyjmuje) 
	list<int> funParamsHelper;

	void yyerror(char const* s);
%}

%token 	PROGRAM
%token 	BEGINN
%token 	END
%token 	VAR
%token 	INTEGER
%token  REAL
%token	FUN
%token 	PROC
%token	IF
%token	THEN
%token	ELSE
%token	DO
%token	WHILE
%token 	RELOP
%token 	MULOP
%token 	SIGN
%token 	ASSIGN
%token	OR
%token 	NOT
%token 	ID
%token 	NUM
%token 	NONE
%token 	DONE

%%

program:
		PROGRAM ID '(' start_identifiers ')' ';' declarations subprogram_declarations
			{
				writeToOutput("lab0:");
			}
		compound_statement
		'.'
			{
				writeToOutputExt("","exit","",";exit ","");
				writeToFile();
			}
		eof
	;

start_identifiers:
		ID
	| start_identifiers ',' ID
	;

identifier_list:
		ID
			{
				checkSymbolExist($1);
				argParamVectorHelper.push_back($1);
			}
	| identifier_list ',' ID
			{
				checkSymbolExist($3);
				argParamVectorHelper.push_back($3);
			}
	;

declarations:
	declarations VAR identifier_list ':' type ';'
		{
			for(auto &index : argParamVectorHelper)
			{
				if($5 == INTEGER || $5 == REAL)
				{
					SymbolTable[index].token = VAR;
					SymbolTable[index].type = $5;
					SymbolTable[index].address = getFirstEmptyAddress(SymbolTable[index].name);
				}
				else
				{
					yyerror("Nieobslugiwany typ");
					//Prerywa dzialanie programu
					YYERROR;
				}
			}
			argParamVectorHelper.clear();
		}
	| //empty
	;

type:
		standard_type
	;

standard_type:
		INTEGER
	| REAL
	;

subprogram_declarations:
		subprogram_declarations subprogram_declaration ';'
	| //empty
	;

subprogram_declaration:
		subprogram_head declarations compound_statement
			{
				//end of fun/proc
				writeToOutputExt("","leave","",";leave ","");
				myGenCode(RETURN,-1,true,-1,true,-1,true);
				printSymbolTable();
				//reset
				clearLocalSymbols();
				isGlobalScope = true;
				startOffsertParamtersFunProcHelper = 8;
			}
	;

subprogram_head:
		FUN ID
			{
				const int functionOffset = 12;
				checkSymbolExist($2);
				SymbolTable[$2].token = FUN;
				isGlobalScope = false;
				startOffsertParamtersFunProcHelper = functionOffset;
				myGenCode(FUN, $2 ,true ,-1 ,true ,-1 ,true);
			}
		arguments
			{
				//$2 -- ID funkcji czyli jej nazwa - index w tablisy symboli
				//paramHelper w takim razie tak na prawde trzyma tylko typy parametrow funkcji

				
				// SymbolTable[$2].parameters = paramHelper;
				// paramHelper.clear();

				SymbolTable[$2].parameters = paramHelper;
				paramHelper.clear();
			}
		':' standard_type
			{
				// bo return variable jest od 8
				const int functionReturnOffset = 8;
				SymbolTable[$2].type = $7;
				insert(SymbolTable[$2].name, VAR, $7, true, functionReturnOffset);
				// int returnVarible = insert(SymbolTable[$2].name ,VAR ,$7);
				// SymbolTable[returnVarible].isReference = true;
				// SymbolTable[returnVarible].address = functionReturnOffset;
			}
		';'
	|	PROC ID
			{
				const int procedureOffset = 8;
				checkSymbolExist($2);
				SymbolTable[$2].token = PROC;
				isGlobalScope = false;
				startOffsertParamtersFunProcHelper = procedureOffset;
				myGenCode(PROC ,$2 ,true ,-1 ,true ,-1 ,true);
			}
		arguments
			{
				// SymbolTable[$2].parameters = paramHelper;
				// paramHelper.clear();

				SymbolTable[$2].parameters = paramHelper;
				paramHelper.clear();
			}
		';'
	;

arguments:
		'(' parameter_list ')'
			{
				//bo adres dlatego 4
				const int argumentSize = 4;
				for(auto &argument : funParamsHelper)
				{
					SymbolTable[argument].address = startOffsertParamtersFunProcHelper;
					startOffsertParamtersFunProcHelper += argumentSize;
				}
				funParamsHelper.clear();
			}
	| //empty
	;

parameter_list:
		identifier_list ':' type
			{
				for(auto &index : argParamVectorHelper){
					SymbolTable[index].isReference = true;
					SymbolTable[index].type = $3;
					paramHelper.push_back($3);

					//Wklepujemy index parametru funkcji ktory wczesniej przechowalismy w argParamVectorHelper
					funParamsHelper.push_front(index);
				}
				argParamVectorHelper.clear();
			}
			//argument(s): type1; argument(s): type2;
	| parameter_list ';' identifier_list ':' type
			{
				for(auto &index : argParamVectorHelper)
				{
					SymbolTable[index].isReference = true;
					SymbolTable[index].type = $5;
					paramHelper.push_back($3);
					funParamsHelper.push_front(index);
				}
				argParamVectorHelper.clear();
			}
	;

compound_statement:
		BEGINN optional_statement END
	;

optional_statement:
		statement_list
	| //empty
	;

statement_list:
 		statement
	| statement_list ';' statement
	;
/* 	Kod pomiedzy begin a end */
/*ogarnac $1,$2, i dalsza czesc z ifami + while */
statement:
/* Przypisanie czegos do zmiennej np. wyrazenia, liczby np. x=5*10, x=g(b) -> wywolanie funkcji  */
		variable ASSIGN simple_expression
			{
				myGenCode(ASSIGN,$1,true,-1, true,$3,true);
			}
			/* Wywolanie funkcji lub procedury lub write lub read */
	| procedure_statement
	| compound_statement
	/* Warto porownac z plikiem gcd dobrze widac jak if dziala */
	| IF expression
	 		{
				int label1 = insertLabel();
				int num = insertNum("0",INTEGER);
				myGenCode(EQ, label1, true, $2, true, num, true);
				$2 = label1;
			}
		THEN statement
		 	{
				int label2 = insertLabel();
				myGenCode(JUMP, label2, true, -1, true, -1, true);
				myGenCode(LABEL, $2, true, -1, true, -1, true);
				$5 = label2;
			}
		ELSE statement
			{
				myGenCode(LABEL, $5, true, -1, true, -1, true);
			}
	| WHILE
	//$2
			{
				int labelStop = insertLabel();
				int labelStart = insertLabel();
				//Tutaj zapisujemy label stop do $$
				$$ = labelStop;
				$1 = labelStart;
				myGenCode(LABEL, labelStart, true, -1, true, -1, true);
			}
			//$$ jest uzywana do przekazywania zmiennych miedzy regułami,
			// aby sie do niej odwolowac uzywamy $2 poniewaz w drugiej sekcji byla przypisana
		expression DO
			{
				//$2 w tym momencie bedzie naszym labelStop
				int id = insertNum("0",INTEGER);
				myGenCode(EQ, $2, true, $3, true, id, true);
			}
		statement
			{
				myGenCode(JUMP, $1, true, -1, true, -1, true);
				myGenCode(LABEL, $2, true, -1, true, -1, true);
			}
	;

variable:
		ID
			{
				checkSymbolExist($1);
				$$ = $1;
			}
	;

procedure_statement:
		ID
			{
				checkSymbolExist($1);
				if(SymbolTable[$1].token == FUN || SymbolTable[$1].token == PROC)
				{
					if(SymbolTable[$1].parameters.size() > 0)
					{
						yyerror("Zla liczba parametrow.");
						YYERROR;
					}
					else
					{
						writeToOutput("\tcall.i #" + SymbolTable[$1].name);
					}
				}
				else
				{
					yyerror("Wymagana nazwa funkcji procedury");
					YYERROR;
				}
			}
			/* Funkcja z paramterami  + predefiniowane funkcji read oraz write
			kod ktory jest ponizej uwzglednia dodatkowo read oraz write
			 czego nie robi ten kod ktory jest w factorze mimo ze prawie sa identyczne
			 dodatkowo uwzglednia wywolanie procedury i nie wywali bledu, natomiast factor wywala blad */
	| ID '(' expression_list ')'
			{
				int wId = lookup("write");
				int rId = lookup("read");
				if($1 == wId || $1 == rId)
				{
					for(auto &index : argParamVectorHelper)
					{
						if($1 == rId)
						//write.r 5
						//write.r 10
						{
							 myGenCode(READ, index, true, -1, true, -1, true );
						}
						if($1 == wId)
						{
							 myGenCode(WRITE, index, true, -1, true, -1, true );
						}
					}
				}
				else
				{
					//Mozliwe ze kod ponizej da sie wyodbrebnic do funkcji
					string funName = SymbolTable[$1].name;
					int finId = lookupForFunction(funName);
					checkSymbolExist(finId);

					if(SymbolTable[finId].token == FUN || SymbolTable[finId].token == PROC)
					{
						if(argParamVectorHelper.size() < SymbolTable[finId].parameters.size())
						{
							yyerror("Nieprawidłowa liczba parametrów.");
							YYERROR;
						}

						int incspCount = 0;
						list<int>::iterator it = SymbolTable[finId].parameters.begin();
						int startPoint = argParamVectorHelper.size() - SymbolTable[finId].parameters.size();

						for(int i = startPoint; i < argParamVectorHelper.size(); i++)
						{
							int id = argParamVectorHelper[i];
							int argumentType = *it;

							if(SymbolTable[argParamVectorHelper[i]].token == NUM)
							{
								int numVar = insertTempSymbol(argumentType);
								myGenCode(ASSIGN,numVar,true, -1, true, argParamVectorHelper[i], true);
								id = numVar;
							}

							int passedType = SymbolTable[id].type;
							if(argumentType != passedType){
								int tempVar = insertTempSymbol(argumentType);
								myGenCode(ASSIGN, tempVar, true, -1, true, id, true);
								id = tempVar;
							}
							myGenCode(PUSH,id,false,-1, true, -1, true);
							incspCount += 4;
							it++;
						}

						int size = argParamVectorHelper.size();
						for(int i = startPoint;i < size; i++)
						{
							argParamVectorHelper.pop_back();
						}
						if(SymbolTable[$1].token == FUN)
						{
							int id = insertTempSymbol(SymbolTable[$1].type);
							myGenCode(PUSH,id,false,-1, true, -1, true);
							incspCount += 4;
							$$ = id;
						}
						myGenCode(CALL, $1,true,-1,true,-1,true);
						stringstream helper;
						helper << incspCount;
						int incspNum = insertNum(helper.str(),INTEGER);
						myGenCode(INCSP,incspNum,true,-1,true,-1,true);
					}
					else
					{
						yyerror("Brak takiej funkcji/procedury.");
						YYERROR;
					}
				}
				argParamVectorHelper.clear();
			}
	;

expression_list:
		expression
			{
				argParamVectorHelper.push_back($1);
			}
	| expression_list ',' expression
			{
				argParamVectorHelper.push_back($3);
			}
	;

/* Przypisujemy do expression wartosc wyrazenia simple expression
lub wartosc wyrazenia logicznego np. 5<6 */
expression:
		simple_expression
			{
				$$ = $1;
			}
			//5<=6, 5<>10 itp.
	| simple_expression RELOP simple_expression
			{
			int labelCorrect = insertLabel();
			myGenCode($2, labelCorrect, true, $1, true, $3, true);
			int result = insertTempSymbol(INTEGER);
			int incorrect = insertNum("0",INTEGER);
			myGenCode(ASSIGN, result, true, -1, true, incorrect, true); // mov.i #0, 5
			int labelDone = insertLabel();
			myGenCode(JUMP, labelDone, true, -1, true, -1, true);
			myGenCode(LABEL, labelCorrect, true, -1, true, -1, true);
			int correct = insertNum("1",INTEGER);
			myGenCode(ASSIGN, result, true, -1, true, correct, true);
			myGenCode(LABEL, labelDone, true, -1, true, -1, true);
			$$ = result;
		}
	;

/* Moze byc termem, lub termem z minusem,
lub pelnym wyrazeniem : simple_expression SIGN term -> 6+g(b)
lub operacja logiczna OR: simple_expression OR term -> 1 OR 0 */
simple_expression:
		term
		//np. -5
	| SIGN term
			{
				if($1 == PLUS)
				{
					 $$ = $2;
				}
				else if($1 == MINUS)
				{
					$$ = insertTempSymbol(SymbolTable[$2].type);
					int t0 = insertNum("0",SymbolTable[$2].type);
					myGenCode($1, $$, true, t0, true, $2, true);
				}
			}
			//np. 5-6 | 6+g(b)
	| simple_expression SIGN term
			{
				$$ = insertTempSymbol(getResultType($1, $3));
				myGenCode($2, $$, true, $1, true, $3, true);
			}
	| simple_expression OR term
			{
				$$ = insertTempSymbol(INTEGER);
				myGenCode(OR, $$, true, $1, true, $3, true);
			}
	;

/* Term moze byc factorem lub jakas operacja np. 5*g(b) g->funkcja */
term:
		factor
		//Nieogarnia "or" w ifach
	| term MULOP factor
			{
				$$ = insertTempSymbol(getResultType($1, $3));
				myGenCode($2, $$, true, $1, true, $3, true);
			}
	;

/* Factor moze byc zmienna, wywolaniem procedury,wywolaniem funkcji bez lub z paramterami, liczba LUb czyms zwiazanym z ifem */
factor:
		variable
			{	//fun bez parametru lub procedura lub zmienna(var)
				int var = $1;
				if(SymbolTable[var].token == FUN)
				{
					if(SymbolTable[var].parameters.size() > 0)
					{
						yyerror("Wywołanie funkcji przyjmującej parametry bez parametrów");
						YYERROR;
					}
					var = insertTempSymbol(SymbolTable[var].type);
					writeToOutput(string("\tpush.i #" + std::to_string(SymbolTable[var].address)));
					writeToOutput(string("\tcall.i #" + SymbolTable[$1].name));
					writeToOutput(string("\tincsp.i #4"));
				}
				else if(SymbolTable[var].token == PROC)
				{
					yyerror("Nie można pobrać wyniku bo procedura go nie zwraca");
					YYERROR;
				}
				$$ = var;
			}
			/* wywolanie funkcji z paramterami */
	| ID '(' expression_list ')'
			{
				string name = SymbolTable[$1].name;
				int index = lookupForFunction(name);
				if(index == -1)
				{
					yyerror("Niezadeklarowana nazwa.");
					YYERROR;
				}
				if(SymbolTable[index].token == FUN)
				{
					if(argParamVectorHelper.size() < SymbolTable[index].parameters.size())
					{
						yyerror("Nieprawidłowa liczba parametrów.");
						YYERROR;
					}

					int incspCount = 0;

					list<int>::iterator it = SymbolTable[index].parameters.begin();
					
					//Sprawdzic czy mozna to zastapic : SymbolTable[index].parameters.size() + 1
					int startPoint = argParamVectorHelper.size() - SymbolTable[index].parameters.size();

					for(int i=startPoint;i<argParamVectorHelper.size();i++)
					{
						int id = argParamVectorHelper[i];

						int argumentType = *it;

							//Sprawdzenie czy argument przekazywany do funkcji jest cyfrą
							//Mozna dac id z wyzej
							if(SymbolTable[argParamVectorHelper[i]].token==NUM)
							{
								int numVar = insertTempSymbol(argumentType);
								myGenCode(ASSIGN,numVar,true, -1, true, argParamVectorHelper[i], true);
								id = numVar;
							}

							int passedType = SymbolTable[id].type;
							//Sprawdzenie czy przekazana liczba do funkcji zgadza sie z typem parametru
							if(argumentType!=passedType)
							{
								int tempVar = insertTempSymbol(argumentType);
								myGenCode(ASSIGN, tempVar, true, -1, true, id, true);
								id = tempVar;
							}
							//Odlozenie na stos parameru + zwiekszenie incsp
							myGenCode(PUSH,id,false,-1, true, -1, true);
							incspCount += 4;
							it++;
						}

						int size = argParamVectorHelper.size();
						for(int i = startPoint;i<size;i++)
						{
							argParamVectorHelper.pop_back();
						}

						//Odlozenie miejsca na stosie dla wyniku funkcji
						int id = insertTempSymbol(SymbolTable[index].type);
						myGenCode(PUSH,id,false,-1, true, -1, true);
						incspCount += 4;
						$$ = id;

						//wywolanie funkcki
						myGenCode(CALL, index,true,-1,true,-1,true);
						stringstream helper;
						helper << incspCount;

						//wywolanie incsp po funkcji
						int incspNum = insertNum(helper.str(),INTEGER);
						myGenCode(INCSP,incspNum,true,-1,true,-1,true);
					}
					else if(SymbolTable[index].token==PROC)
					{
						yyerror("Procedury nie zwracają wartości, nie można wykonać operacji!");
						YYERROR;
					}
					else
					{
						yyerror("Nie znaleziono takiej funkcji/procedury.");
						YYERROR;
					}
				}
	| NUM
	| '(' expression ')'
			{
				$$ = $2;
			}
			//Sprawdzic do czego to jest czy to jest od if-ów?   while not (i < j)
	| NOT factor
			{
				int labelFactorEqualZero = insertLabel();
				int zeroId = insertNum("0",INTEGER);
				myGenCode(EQ,labelFactorEqualZero, true, $2, true,  zeroId, true);

				int varWithNotResult = insertTempSymbol(INTEGER);
				myGenCode(ASSIGN,varWithNotResult, true, -1, true, zeroId, true);

				int labelFinishNOT = insertLabel();
				myGenCode(JUMP, labelFinishNOT, true, -1, true, -1, true);
				myGenCode(LABEL, labelFactorEqualZero, true, -1, true, -1, true);

				int num1 = insertNum("1",INTEGER);
				myGenCode(ASSIGN,varWithNotResult, true, -1, true, num1, true);
				myGenCode(LABEL, labelFinishNOT, true, -1, true, -1, true);
				$$ = varWithNotResult;
			}
	;

eof:
		DONE
			{
				return 0;
			}
  ;

%%

void yyerror(char const *s)
{
	printf("Blad w linii %d: %s \n",lineno, s);
}
